import sys
from scipy import ndimage, misc
from tkinter import *
from PIL import ImageTk, Image
import numpy as np


def updatePicture(img_data, label):
    pil_img = Image.fromarray(img_data)
    tkinter_img = ImageTk.PhotoImage(pil_img)
    label.configure(image=tkinter_img)
    label.image = tkinter_img


def rotate_left(img_data, label):
    img_data = np.rot90(img_data, 1)
    updatePicture(img_data, label)


def rotate_right(img_data, label):
    img_data = np.rot90(img_data, 3)
    updatePicture(img_data, label)


def mirror(img_data, label):
    img_data = img_data[:, ::-1]
    updatePicture(img_data, label)


def inverse(img_data, label):
    img_data = 255 - img_data
    updatePicture(img_data, label)


def grayscale(img_data, label):
    img_data = (0.299, 0.587, 0.114) * img_data
    img_data = np.sum(img_data, axis=2)
    updatePicture(img_data, label)


def brighten(img_data, label):
    tmp = np.copy(img_data)
    tmp[:,:] = tmp[:,:] + (256 - tmp[:,:]) * 0.3
    updatePicture(tmp, label)


def darken(img_data, label):
    tmp = np.copy(img_data)
    tmp[:,:] = tmp[:,:] * 0.3
    updatePicture(tmp, label)


def highlight_edges(img_data, label):
    tmp = np.copy(img_data)
    w, h, c = img_data.shape
    for x in range(1, w - 1):
        for y in range(1, h - 1):
            for z in range(c):
                col = (
                    + 9 * img_data[x, y, z]
                    - img_data[x - 1, y, z]
                    - img_data[x + 1, y, z]
                    - img_data[x, y + 1, z]
                    - img_data[x, y - 1, z]
                    - img_data[x - 1, y + 1, z]
                    - img_data[x - 1, y - 1, z]
                    - img_data[x + 1, y - 1, z]
                    - img_data[x + 1, y + 1, z])
                if col < 0:
                    col = 0
                if col > 255:
                    col = 255
                tmp[x, y, z] = col
    updatePicture(tmp, label)


def close_window(window):
    window.destroy()
    exit()


def main():
    window = Tk()
    window.title('Photo Editor')
    window.configure(background='black')
    try:
        #Open image using PIL
        pil_img = Image.open(sys.argv[1])
    except:
        print('Invalid path of picture or missing argument.')
        quit()
    #3D numpy array - height X width X RGB (convert from PIL image)
    img_data = np.array(pil_img)
    #Conversion to Tkinter format
    tkinter_img = ImageTk.PhotoImage(pil_img)
    #Tkinter label showing picture to GUI
    label = Label(window, image=tkinter_img, bg='black')
    label.pack(side="bottom")
    Button(window, text='NO EFFECTS', width=12, command=lambda:updatePicture(img_data, label)).pack(side="left")
    Button(window, text='Rotate left', width=12, command=lambda:rotate_left(img_data, label)).pack(side="left")
    Button(window, text='Rotate right', width=12, command=lambda:rotate_right(img_data, label)).pack(side="left")
    Button(window, text='Mirror', width=12, command=lambda:mirror(img_data, label)).pack(side="left")
    Button(window, text='Inverse', width=12, command=lambda:inverse(img_data, label)).pack(side="left")
    Button(window, text='Grayscale', width=12, command=lambda:grayscale(img_data, label)).pack(side="left")
    Button(window, text='Brighten', width=12, command=lambda:brighten(img_data, label)).pack(side="left")
    Button(window, text='Darken', width=12, command=lambda:darken(img_data, label)).pack(side="left")
    Button(window, text='Highlight edges', width=12, command=lambda:highlight_edges(img_data, label)).pack(side="left")
    Button(window, text='Exit', width=12, command=lambda:close_window(window)).pack(side="left")
    window.mainloop()

if __name__ == "__main__":
    main()
